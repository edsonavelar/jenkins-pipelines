# executenotebook.py
#!/usr/bin/python3
import json
import requests
import os
import sys
import getopt
import time

def main():
    workspace = ''
    token = ''
    clusterid = ''
    notebook = ''
    workspacepath = ''
    outfilepath = ''

    print("Execute Notebook was Called!")

    try:
        opts, args = getopt.getopt(sys.argv[1:], 'hs:t:c:lwo',
                                    ['workspace=', 'token=', 'clusterid=', 'notebook=', 'workspacepath=', 'outfilepath='])
    except getopt.GetoptError:
        print(
            'exe_one_notebook.py -s <workspace> -t <token>  -c <clusterid> -l <notebook> -w <workspacepath> -o <outfilepath>)')
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print(
                'executenotebook.py -s <workspace> -t <token> -c <clusterid> -l <notebook> -w <workspacepath> -o <outfilepath>')
            sys.exit()
        elif opt in ('-s', '--workspace'):
            workspace = arg
        elif opt in ('-t', '--token'):
            token = arg
        elif opt in ('-c', '--clusterid'):
            clusterid = arg
        elif opt in ('-l', '--notebook'):
            notebook = arg
        elif opt in ('-w', '--workspacepath'):
            workspacepath = arg
        elif opt in ('-o', '--outfilepath'):
            outfilepath = arg

    print('-s is ' + workspace)
    print('-t is ' + token)
    print('-c is ' + clusterid)
    print('-l is ' + notebook)
    print('-w is ' + workspacepath)
    print('-o is ' + outfilepath)
    # Generate array from walking local path

    # workpath removes extension
    fullworkspacepath = workspacepath + '/' + notebook
    
    name = notebook

    print('Running job for:' + fullworkspacepath)
    values = {'run_name': name, 'existing_cluster_id': clusterid, 'timeout_seconds': 3600, 'notebook_task': {'notebook_path': fullworkspacepath}}

    resp = requests.post(workspace + '/api/2.0/jobs/runs/submit',
                        data=json.dumps(values), auth=("token", token))
    runjson = resp.text
    print("runjson:" + runjson)
    d = json.loads(runjson)
    runid = d['run_id']

    i=0
    waiting = True
    while waiting:
        time.sleep(10)
        jobresp = requests.get(workspace + '/api/2.0/jobs/runs/get?run_id='+str(runid),
                        data=json.dumps(values), auth=("token", token))
        jobjson = jobresp.text
        print("jobjson:" + jobjson)
        j = json.loads(jobjson)
        current_state = j['state']['life_cycle_state']
        runid = j['run_id']
        if current_state in ['TERMINATED', 'INTERNAL_ERROR', 'SKIPPED'] or i >= 12:
            break
        i=i+1
        
    print(json.dumps(j))
    '''if outfilepath != '':
        file = open(outfilepath + '/' +  str(runid) + '.json', 'w')
        file.write(json.dumps(j))
        file.close()'''


if __name__ == '__main__':
  main()