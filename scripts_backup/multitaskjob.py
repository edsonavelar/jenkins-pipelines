#!/usr/bin/python3
import json
import requests
import os
import sys
import getopt
import time

def main():
    
    usage = 'multitaskjob.py -s <db_workspace> -t <token>  -c <clusterid> -j <jobfile> -w <workpath>)'
    argv = sys.argv[1:]

    try:
        opts, args = getopt.getopt(argv, 'dw:t:c:wp:ot', ['db_workspace=', 'token=','jobfile=','workpath='])
    except getopt.GetoptError:
        print(usage)
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print('executenotebook.py -s <db_workspace> -t <token> -j <jobfile> -w <workpath>')
            sys.exit()
        elif opt in ('-s', '--db_workspace'):
            db_workspace = arg
        elif opt in ('-t', '--token'):
            token = arg
        elif opt in ('-j', '--jobfile'):
            jobfile = arg
        elif opt in ('-w', '--workpath'):
            workpath = arg


    print('-s is ' + db_workspace)
    print('-t is ' + token)
    print('-j is '+jobfile)
    print('-w is '+workpath)


    with open(jobfile, 'r') as myfile:
        data=myfile.read()

    values = json.loads(data)

    for i in values['tasks']:
        i['notebook_task']['notebook_path'] = workpath + i['notebook_task']['notebook_path']

    print(values)

    resp = requests.post(db_workspace + '/api/2.1/jobs/runs/submit', data=json.dumps(values), auth=("token", token))
    runjson = resp.text
    print("runjson:" + runjson)
    d = json.loads(runjson)

    print("[JSON]",d)
    runid = d['run_id'] 

    i=0
    waiting = True
    while waiting:
        time.sleep(10)
        jobresp = requests.get(db_workspace + '/api/2.1/jobs/runs/get?run_id='+str(runid),
                        data=json.dumps(values), auth=("token", token))
        jobjson = jobresp.text
        print("jobjson:" + jobjson)
        j = json.loads(jobjson)
        current_state = j['state']['life_cycle_state']
        runid = j['run_id']
        if current_state in ['TERMINATED', 'INTERNAL_ERROR', 'SKIPPED'] or i >= 12:
            break
        i=i+1

if __name__ == '__main__':
  main()