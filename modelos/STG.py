# Databricks notebook source
from pyspark.ml.classification import LogisticRegression, LogisticRegressionModel
import pickle
from sklearn.model_selection import train_test_split
from sklearn import metrics
from pyspark.ml.feature import VectorAssembler
from pyspark.ml import Pipeline
from pyspark.sql.functions import col

# COMMAND ----------

# MAGIC %md #  Load and paths functions 

# COMMAND ----------

def path_to_data_train():
  """
  path to the data
  Output: path
  """
  return "/FileStore/tables/diabetes.csv"

# COMMAND ----------

def path_to_data_execute():
  """
  path to the data
  Output: path
  """
  return "/FileStore/tables/diabetes.csv"

# COMMAND ----------

def load_data(path_to_data):
    """
    Load the data
    Input: path to the data
    Output: dataframe
    """
    df = df=spark.read.format("csv").option("header","true").load(path_to_data)
    return df

# COMMAND ----------

# MAGIC %md # Dataprep functions

# COMMAND ----------

def data_prep_1(dataset):
  """
  Data preparation
  Input: dataset
  Output: dataset with selected columns
  """
  dataset = dataset.withColumnRenamed('Pregnancies', 'pregnant')\
  .withColumnRenamed('Glucose', 'glucose')\
  .withColumnRenamed('BloodPressure', 'bp')\
  .withColumnRenamed('SkinThickness', 'skin')\
  .withColumnRenamed('Insulin', 'insulin')\
  .withColumnRenamed('BMI', 'bmi')\
  .withColumnRenamed('DiabetesPedigreeFunction', 'pedigree')\
  .withColumnRenamed('Age', 'age')\
  .withColumnRenamed('Outcome', 'label')
  return dataset

# COMMAND ----------

def data_prep_2(dataset):
  """
  Data preparation
  Input: Path to the data
  Output: dataset processado
  """
  dataset = dataset.selectExpr("cast(pregnant as double) pregnant", "cast(glucose as double) glucose", "cast(bp as double) bp", "cast(skin as double) skin", "cast(insulin as double) insulin","cast(bmi as double) bmi","cast(pedigree as double) pedigree","cast(age as double) age","cast(label as double) label")
  return dataset

# COMMAND ----------

def data_prep_3(dataset):
  """
  Data preparation
  Input: Path to the data
  Output: dataset processado
  """
  cols = dataset.columns
  cols = cols[:8]
  assembler = VectorAssembler(inputCols=cols, outputCol="features")
  dataset = assembler.transform(dataset)
  return dataset

# COMMAND ----------

def data_prep(path_to_data):
  """
  Data preparation
  Input: Path to the data
  Output: dataset processado
  """
  dataset = load_data(path_to_data)
  dataset = data_prep_1(dataset)
  dataset = data_prep_2(dataset)
  dataset = data_prep_3(dataset)
  return dataset

# COMMAND ----------

def schema_prep(path_to_data):
  """
  Schema
  Input: Path to the data
  Output: schema
  """
  #data = load_data(path_to_data)
  df = data_prep(path_to_data)
  rows, columns, types = df.count(), len(df.columns), df.dtypes
  return rows, columns, types

# COMMAND ----------

# MAGIC %md # Train function

# COMMAND ----------

def train_model(path_to_data_train):
  """
  train and persist model to path
  Input: path to the data
  Output: accuracy and AUC values
  """
  path_to_model = "/dbfs/tmp/LRmodel"
  dataset = data_prep(path_to_data_train)
  train, test = dataset.randomSplit([0.7, 0.3], seed = 2022)
  lr = LogisticRegression(featuresCol = "features", labelCol = 'label', maxIter=10)
  lrModel = lr.fit(train)
  lrModel.write().overwrite().save(path_to_model)
  trainingSummary = lrModel.summary
  return trainingSummary.accuracy, trainingSummary.areaUnderROC, path_to_model

# COMMAND ----------

# MAGIC %md # Dataprep row data function

# COMMAND ----------

def dataprep_row_data(path_to_data):
  """
  Apply dataprep to row data
  Input: path to data
  Output: dataframe
  """
  dataset = data_prep(path_to_data)
  return dataset

# COMMAND ----------

# MAGIC %md # Load model function

# COMMAND ----------

def load_model(path_to_model):
  """
  upload the model
  Input: path to model
  Output: 
  """
  loaded_model = LogisticRegressionModel.load(path_to_model)
  loaded = 'OK'
  return loaded_model, loaded

# COMMAND ----------

# MAGIC %md # Apply model to row data function

# COMMAND ----------

def apply_model(path_to_model, path_to_data):
  """
  Apply the model
  Input: row data, path to model
  Output: prediction
  """
  loaded_model = load_model(path_to_model)[0]
  dataset = data_prep(path_to_data)
  prediction = loaded_model.transform(dataset)
  return prediction

# COMMAND ----------

# MAGIC %md # Unit test functions: Dataprep

# COMMAND ----------

def test_data_prep_1():
  input_df = spark.createDataFrame(
      data=[["1","2","3","4","5","6","7","8","9"]],
      schema=['Pregnancies','Glucose','BloodPressure','SkinThickness','Insulin','BMI','DiabetesPedigreeFunction','Age','Outcome'])

  transformed_df = data_prep_1(input_df).toPandas()

  expected_df = spark.createDataFrame(
      data=[["1","2","3","4","5","6","7","8","9"]],
      schema=['pregnant','glucose','bp','skin','insulin','bmi','pedigree','age','label'])
  expected_df = expected_df.toPandas()

  return assert_frame_equal(transformed_df, expected_df)

# COMMAND ----------

def test_data_prep_2():
  input_df = spark.createDataFrame(
      data=[["1","2","3","4","5","6","7","8","9"]],
      schema=['pregnant','glucose','bp','skin','insulin','bmi','pedigree','age','label'])

  transformed_df = data_prep_2(input_df).toPandas()

  expected_df = spark.createDataFrame(
      data=[[1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0]],
      schema=['pregnant','glucose','bp','skin','insulin','bmi','pedigree','age','label'])
  expected_df = expected_df.toPandas()

  return assert_frame_equal(transformed_df, expected_df)

# COMMAND ----------

def test_data_prep_3():
  input_df = spark.createDataFrame(
      data=[[1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0]],
      schema=['pregnant','glucose','bp','skin','insulin','bmi','pedigree','age','label'])

  transformed_df = data_prep_3(input_df).toPandas()

  expected_df = spark.createDataFrame(
      data=[[1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0, 9.0, [1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0]]],
      schema=['pregnant','glucose','bp','skin','insulin','bmi','pedigree','age','label','features'])
  expected_df = expected_df.toPandas()

  return assert_frame_equal(transformed_df, expected_df)

# COMMAND ----------

# MAGIC %md # Unit test function: Load model

# COMMAND ----------

def test_load_model():
  
  input = "/dbfs/tmp/LRmodel"
  load_type = type(load_model(input)[1])
  
  expected_load_type = type("str")
  
  assert load_type == expected_load_type

# COMMAND ----------

# MAGIC %md # Unit test function: train 

# COMMAND ----------

def test_train():
  
  input = path_to_data_train()
  train_type = type(train_model(input))
  
  expected_train_type = type((1,2,3))
  
  assert train_type == expected_train_type

# COMMAND ----------

# MAGIC %md # Metadados

# COMMAND ----------

def metadados():
  """
  retorna um dic contendo os metadados do modelo
  """
  metadados_model = {
    "path_to_data_train":["/dbfs/" + path_to_data_train()],
    "table_use_1" :["credito.clientes"],
    "table_use_2" :["credito.devedores"],
    "name_modelo" : ["regressao_348_credito"], 
    "score_training_acc" : [train_model(path_to_data_train())[0]],
    "AUC" : [train_model(path_to_data_train())[1]],
    "path_to_model" :[train_model(path_to_data_train())[2]],
    "path_to_data_excecute" : [path_to_data_execute()],
    "LAB" : ["true"],
    "author_model" : ["Vini manolo"], 
    "libs" : [4],
    "dataprep" : ["dataprep_1, dataprep_2, dataprep_3"],
    "test_dataprep" : ["test_data_prep_1(), test_data_prep_2(), test_data_prep_3()"],
    "test_load_model" : ["test_load_model()"],
    "apply_model" : ["apply_model"],
    "test_train" : ["test_train()"]
  }
  return metadados_model

# COMMAND ----------

# MAGIC %md # Saving DIC

# COMMAND ----------

dic = metadados()
with open('dic.pkl', 'wb') as f:
    pickle.dump(dic, f)
