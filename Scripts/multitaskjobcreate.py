#!/usr/bin/python3
import json
import requests
import os
import sys
import getopt
import time
import logging

# create logger
logging.basicConfig(filename='multitaskjob.log', level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

logger = logging.getLogger('multitaskjogcreate')
##logger.setLevel(logging.DEBUG)

# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
ch.setFormatter(formatter)

# add ch to logger
logger.addHandler(ch)
import time

from METADATA import METADATA as meta 

dag = """
{
  "name": "",
  "max_concurrent_runs": 1,
  "email_notifications": {
    "on_start": [
   
    ],
    "on_success": [
    
    ],
    "on_failure": [
      
    ]
    },

  "tasks": [
    {
      "task_key": "unit_test",
      "description": "Clean and prepare the data",
      "notebook_task": {
        "notebook_path": "TU"
      },
      "existing_cluster_id": "1228-220746-bqqkddxs",
      "timeout_seconds": 3600,
      "max_retries": 1,
      "retry_on_timeout": true
      
    },
    {
      "task_key": "to_prod",
      "description": "Perform an analysis of the data",
      "notebook_task": {
        "notebook_path": "PRO"
      },
      "depends_on": [
        {
          "task_key": "unit_test"
        }
      ],
      "existing_cluster_id": "1228-220746-bqqkddxs",
      "timeout_seconds": 3600,
      "max_retries": 1,
      "retry_on_timeout": true
    }
  ]
}

"""
def main():
    """ Este script é responsável por criar o multitask job dentro do databricks 
        
        usage: 'multitaskjob.py -s <db_workspace> -t <token>  -c <clusterid> -j <jobfile> -w <workpath> -n <jobname>)'

        Atributos
        ------------
        db_workspace: str
            URL do workspace no databricks. Ex: "https://adb-6840195589605290.10.azuredatabricks.net"
        token: str
            Token do databricks. Ex: bqqkddxsdfsfsdfsdfsfsdf
        clusterid: str
            Identificador do cluster. Ex: "1228-220746-bqqkddxs"
        jobfile: str
            Caminho para arquivo que descreve jobfile (.json)
        workpath: str
            Caminho no databricks para onde os artefatos irão Ex. /Shared
        jobname: str 
            Nome customizado do 

    """

    jobname=""
    on_failure = ""
    on_success = ""

    usage = 'multitaskjob.py -s <db_workspace> -t <token>  -c <clusterid> -j <jobfile> -w <workpath> -n <jobname>)'
    argv = sys.argv[1:]

    try:
        opts, args = getopt.getopt(argv, 's:t:j:w:n', ['db_workspace=', 'token=','jobfile=','workpath=','jobname='])
    except getopt.GetoptError:
        print(usage)
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print(usage)
            sys.exit()
        elif opt in ('-s', '--db_workspace'):
            db_workspace = arg
        elif opt in ('-t', '--token'):
            token = arg
        elif opt in ('-j', '--jobfile'):
            jobfile = arg
        elif opt in ('-w', '--workpath'):
            workpath = arg
        elif opt in ('-n', '--jobname'):
            jobname = arg
      
    start_time = time.time()

    logger.debug('*'*100)
    logger.debug('*'*45 + 'DAG BEGIN '+'*'*45)
    logger.debug('*'*100)

    ##with open(jobfile, 'r') as myfile:
    #    data=myfile.read()

    values = json.loads(dag)

    values['name'] = jobname

    if 'on_success' in meta:
        for email in meta['on_success'][0].split(','):
            values['email_notifications']['on_success'].append(email)
            logger.debug(F'email_notifications on sucess: {email}')


    if 'on_failure' in meta:
        for email in meta['on_failure'][0].split(','):
            values['email_notifications']['on_failure'].append(email)
            logger.debug(F'email_notifications on on_failure: {email}')



    for i in values['tasks']:
        i['notebook_task']['notebook_path'] = workpath + i['notebook_task']['notebook_path']

    print(values)
    logger.debug("[METADATA] "+ str(values) )
    logger.debug("Creating the Job")

    resp = requests.post(db_workspace + '/api/2.1/jobs/create', data=json.dumps(values), auth=("token", token))
    runjson = resp.text
    
    d = json.loads(runjson)   
    print("[job id] ",d) 
    jobid = str( d['job_id'] )

    logger.debug(F"JOB Created with id: {jobid}")

    resp = requests.post(db_workspace + '/api/2.1/jobs/run-now', data="{\"job_id\": "+jobid+"}", auth=("token", token))

    runjson = resp.text
    d = json.loads(runjson)  

    print("[run id] ",d)
    runid = str( d['run_id'] )

    logger.debug(F"JOB is running with id: {runid}")
    

    i=0
    waiting = True
    error = False
    error_message = ""
    while waiting:
        time.sleep(10)
        
        url_request = db_workspace + '/api/2.1/jobs/runs/get?run_id='+str(runid)

        logger.debug("Get Job Info")
        logger.debug("url_request: "+url_request)

        jobresp = requests.get(url_request,
                        data=json.dumps(values), auth=("token", token))
        jobjson = jobresp.text
        print("jobjson:" + jobjson)
        
        logger.debug(jobjson)

        j = json.loads(jobjson)
        current_state = j['state']['life_cycle_state']
        runid = j['run_id']
        if current_state in ['TERMINATED'] or i >= 600:
            break

        if current_state in ['INTERNAL_ERROR', 'SKIPPED'] or i >= 600:
            state_message = j['state']['state_message']
            error_message = F"[Multi Task Job] Finished with Error | Status = {current_state} | Desc = {state_message}"

            logger.error(error_message)
            error = True
            
            break
        i=i+1

    
    seg = time.time() - start_time
    min = seg/60
    logger.debug(F"--- Execution Time ---")
    logger.debug(F"--- {seg} seconds ---")
    logger.debug(F"--- {min} minutes ---")

    if not error:
        logger.debug(F"Execution Status: [SUCCESS]")
        logger.debug('*'*45 + ' DAG  ENG '+'*'*45)
    else:
        logger.debug(F"Execution Status: [FAILURE]")
        logger.debug('*'*45 + ' DAG  ENG '+'*'*45)
        raise Exception(error_message)

if __name__ == '__main__':
  main()